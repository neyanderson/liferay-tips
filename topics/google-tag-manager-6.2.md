
## Appying Google Tag Manager on Liferay 6.2

### About GTM scripts

The Google Tag Manager container snippet is a small piece of JavaScript and non-JavaScript code that you paste into your pages. It enables Tag Manager to fire tags by inserting gtm.js into the page (or through the use of an iframe when JavaScript isn't available).

To implement Google Tag Manager on your website:

Copy the following JavaScript and paste it as close to the opening ``<head>`` tag as possible on every page of your website, replacing GTM-XXXX with your container ID:

```
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-XXXX');</script>
<!-- End Google Tag Manager -->
```

Copy the following snippet and paste it immediately after the opening <body> tag on every page of your website, replacing GTM-XXXX with your container ID:

```
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-XXXX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
```
Many tag management operations can be achieved by just doing the basic code installation, but if you'd like to have finer grain control over tag events or data, you may want to use some customization using our asynchronous methods.

Source: https://developers.google.com/tag-manager/quickstart

By this demonstration we are not use the variable data layer.

### Registry your GTM Code.

Example: GTM-XXXXXXX

### Liferay

#### Create a new analytic option.

- Go to → Control Panel → Portal Settings → Miscellaneous → Analytics
- Add one line at the end with value: 'gtm' and  click in 'Save'.
- Go to Site Settings → Advanced → Analytics
- Paste the code below at gtm textarea field. Remember to change the GTM code registered:

```
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-XXXXXXX');</script>
<!-- End Google Tag Manager -->
```
- Click in 'Save'.

####  Adding GTM Snippet

Assuming you're using a custom liferay theme in your site or portal. 

- Edit file liferay-look-and-feel.xml at [theme root]/src/WEB-INF/.
- Include a setting tag with type=textarea tag like that:
```
<?xml version="1.0"?>
<!DOCTYPE look-and-feel PUBLIC "-//Liferay//DTD Look and Feel 6.2.0//EN" "http://www.liferay.com/dtd/liferay-look-and-feel_6_2_0.dtd">

<look-and-feel>
	<compatibility>
		<version>6.2.0+</version>
	</compatibility>
	<theme id="axa-benefits-amex-theme" name="axa-benefits-amex-theme">
		<settings>
			<setting configurable="true" key="anotherExtraSetup" value="" />
			<setting configurable="true" key="GoogleTagManagerSnippet" value="" type="textarea" />
		</settings>
	</theme>
</look-and-feel>
```

- In this case I'm using name ``GoogleTagManagerSnippet``
- Compile and publish/deploy your theme.
- Go to Site administration → Pages → Public Pages
- Look for GoogleTagManagerSnippet textarea field and fill with this content. Again, remember to change the GTM code registered:
```
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-XXXXXXX"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
```
- Click in 'Save'.
- If you also want to collect data from your private's pages. Go to Site administration → Pages → Private Pages and repeat.

References: 
- https://portal.liferay.dev/docs/6-2/user/-/knowledge_base/u/liferay-monitoring-using-google-analytics
